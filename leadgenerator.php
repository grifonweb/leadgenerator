<?php
/**
 * @package Forms & Leads Generator plugin for WP
 * @version 0.0.46b
 */
/*
Plugin Name: Forms & Leads Generator plugin for WP
Author: Aleksey Khvorostov & Ivan Zhukov
Version: 0.1
Author URI: https://grifon-it.ru/pages/blog/useful/forms-i-leads-generator-plugin-for-wordpress
*/


// Make definitions for all plugin
define( 'LG__JS_DIR', __DIR__.'/includes/js/' );
define( 'LG__JS_URL', plugin_dir_url( __FILE__ ).'includes/js/' );

define( 'LG__CSS_DIR', __DIR__.'/includes/css/' );
define( 'LG__CSS_URL', plugin_dir_url( __FILE__ ).'includes/css/' );

define( 'LG__CLASSES_DIR', __DIR__.'/includes/classes/' );
define( 'LG__CLASSES_URL', plugin_dir_url( __FILE__ ).'includes/classes/' );

define( 'LG__TEMPLATES_DIR', __DIR__.'/includes/templates/' );
define( 'LG__TEMPLATES_URL', plugin_dir_url( __FILE__ ).'includes/templates/' );

define( 'LG__ADAPTERS_DIR', __DIR__.'/adapters/' );

// Include required classes
require_once(LG__CLASSES_DIR.'leadgenerator.class.php');

// Entry point
leadGenerator::getInstance();
register_activation_hook(__FILE__, array( 'leadGenerator', 'install'));