<?php

class lg_Shortcode_Processor {
    
    public function process($atts){
        global $wpdb;
        
        /*
        $attributes = shortcode_atts(array(
            'item' => 'TITLE',
        ), $atts);        
        */
        
        $data = leadGenerator::getInstance()->load_form($atts['ident']);
        if (!(bool)$data['active']) return '';
        $hash = $data['hash'];
        $fields = $data['fields'];
        $adapter = $data['adapter'];
        $qs = $_SERVER['QUERY_STRING'];
                
        $adapter = &leadGenerator::getInstance()->get_adapter_by_hash($hash);
        
        if (!empty($data)) {
            include LG__TEMPLATES_DIR.'frontend.form.template';
        }
    }    
         
}
