<?php

require_once dirname(__FILE__).'/shortcodes.class.php';

// Singletone pattern
class leadGenerator {
    
    public static    $name      = "Генератор форм"; 
    public static    $version   = "0.0.46b"; 
    public static    $fullname  = "Forms & Leads Generator plugin for WP";

    // Set table name
    public static    $tablename         = "wplg_leadforms";
    protected        $adapters        = array();
    protected static $default_adapter = 'lg_Default_Adapter';
    
    private          $sc_processor      = null;
    private static   $_instance         = null;
    private function __construct() {
        
        $this->load_available_adapters();
        
        // Make menu
        add_action('admin_menu', array($this, 'render_menu'));
        
        // Register all assets
        add_action('init', array($this, 'register_assets'));
        
        // Add assets on frontend
        add_action('wp_head', array($this, 'add_frontend_css'));
        add_action('wp_footer', array($this, 'add_frontend_js'));

        // Add assets backend
        add_action('lg_adm_home', array($this, 'add_adm_forms_assets'));
        
        // Add actions for ajax
        add_action('wp_ajax_adapter', array($this, 'ajax_request_adapter'));
        
        // Add ajax action for form processing
        add_action('wp_ajax_form', array($this, 'ajax_process_form'));

        // Add ajax action for frontend processing
        add_action('wp_ajax_frontend', array($this, 'ajax_process_frontend'));        
        
        // Create new SC Processor instance
        $this->sc_processor = new lg_Shortcode_Processor();
        
        // Register form shortcode
        add_shortcode('lg_form', array($this->sc_processor, 'process'));        
    }
    
    protected function __clone() {}
    protected function __wakeup() {}   
    
    // return instance of singleton
    static public function getInstance() {
        if(is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    // Install new DB for Lead Generator data
    public function install () {               
        if( get_option( "lg_db_version" ) != self::$version ) {
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            $sql = "CREATE TABLE " . self::$tablename . " (
                id          mediumint(9) NOT NULL AUTO_INCREMENT,
                added       timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                hash        varchar(40) NOT NULL,
                ident       tinytext NOT NULL,
                active      tinyint(1) NOT NULL,
                adapter     text,
                title       varchar(64) NOT NULL,
                fields      text,
                description varchar(255),
                agree       tinyint(1) NOT NULL,
                agreement   varchar(255),
                UNIQUE KEY  id (id));
            ";            
            
            dbDelta($sql);
            if (!get_option( "lg_db_version" )) {
                add_option("lg_db_version", self::$version);
            } else {
                update_option("lg_db_version", self::$version);
            }
        }
    } 
    
    
    protected function load_available_adapters(){
        require_once(LG__ADAPTERS_DIR.'default.adapter.php');
        
        // Create default adapter
        $instance = new lg_Default_Adapter;
        $this->adapters[$instance->get_hash()] = $instance;
        
        // Additional adapters
        foreach(scandir(LG__ADAPTERS_DIR) as $file) {
            $n = explode('.', $file); // Explode by dot
            if( $file == "." || $file == ".." // Directory handlers skipping ( || $file == "default.adapter.php" )
                || ($n[count($n)-2].$n[count($n)-1]  !== 'adapterphp')) continue; // Check type of files
            if (!file_exists(LG__ADAPTERS_DIR.$file)) continue; // Check file exists
                require_once(LG__ADAPTERS_DIR.$file); // Include adapters to forkflow
                for($i = 0; $i < count($n)-2; $i++){ // Preparing classnames
                    $classname .= ucwords(strtolower($n[$i])); // Upper every first letter
                }
                
                $classname = 'lg_'.$classname.'_Adapter';
                
                // Check and create Instance 
                if(class_exists($classname)){
                    $instance = new $classname;
                    if ($instance instanceof lg_Default_Adapter) {
                        $this->adapters[$instance->get_hash()] = $instance;
                    }
                }
            unset($classname);
        }
        
    }
    
    // Returns array of available adapters for everywhere
    public static function get_available_adapters(){
        return $this->adapters;
    }
    
    // Returns adapter if available
    public static function get_adapter_by_hash($hash){
            $adapter = self::getInstance()->adapters[$hash];
            if (!($adapter instanceof lg_Default_Adapter)) {
                $adapter = current(self::getInstance()->adapters);
                if (!($adapter instanceof lg_Default_Adapter)) {
                    return false;
                }
            } 
        return $adapter;
    }    
    
    // AJAX Wrapper for request_adapter method of this class
    public static function ajax_request_adapter(){    
        // Check nonce
        if( ! wp_verify_nonce( $_REQUEST['nonce'], 'ajax_validation' ) ) wp_die();
        
        $errors = array(); // Empty errors array
        $data =  self::getInstance()->request_adapter($errors);
        
        echo self::getInstance()->ajax_response($data, $errors); 
        wp_die();
    }
    
    public function request_adapter(&$errors = array()){
        
        // Check capability
        if (!current_user_can('manage_options')) {
            $errors[] = 'Insufficient rights to perform an operation';
        }        
        
        if (!count($errors)) {// No errors only
            $adapter = self::getInstance()->adapters[$_REQUEST['hash']];
            if ($adapter instanceof lg_Default_Adapter) {
                $data = $adapter->process($_REQUEST['do'], $errors);        
            } else {
                $errors[] = 'Adapter was not found';
            }
        }
        
        return $data;
    }
    
    public function ajax_response($data, &$errors){
        $response['data'] = $data;
        $response['errors'] = $errors;
        return json_encode($response);
    }
    
    public function register_assets(){
        wp_register_style('frontend.css', LG__CSS_URL.'frontend.css');
        wp_register_style('backend.forms.css', LG__CSS_URL.'backend.forms.css');
        
        wp_register_script('frontend.js', LG__JS_URL.'frontend.js');
        wp_register_script('backend.forms.js', LG__JS_URL.'backend.forms.js');
    } 
    
    public function add_frontend_css(){
        wp_enqueue_style('frontend.css');
    } 
    
    public function add_frontend_js(){
        wp_enqueue_script('frontend.js');

	wp_localize_script('frontend.js',  'ajax', 
                                            array(
                                                'url' => admin_url('admin-ajax.php'),
                                                'nonce' => wp_create_nonce('ajax_frontend_validation'),
                                            )
                            );         
    }    
    
    public function add_adm_forms_assets(){
        wp_enqueue_style('backend.forms.css');
        wp_enqueue_script('backend.forms.js');
        
	wp_localize_script('backend.forms.js', 'ajax', 
                                                        array(
                                                            'url' => admin_url('admin-ajax.php'),
                                                            'nonce' => wp_create_nonce('ajax_validation'),
                                                        )
                            );        
    }
       
    // Make admin menu
    public function render_menu(){
        add_plugins_page(
                leadGenerator::$fullname." (version: ".leadGenerator::$version.")",
                _(leadGenerator::$name),
                'manage_options',
                'lg_homepage',
                array( $this, 'render_home_page' )
            );
    }    
    
    public function get_forms_count($active = null){
        global $wpdb;
        $result = $wpdb->get_results( "SELECT * FROM ".self::$tablename);
    }

    public function render_home_page(){
        global $wpdb;
        
        // Execute hook on home page
        do_action('lg_adm_home');
        
        // Check capability
        if (!current_user_can('manage_options')) return print('<h1 class="centered error">Access denied</h1>');
        
        // Load forms data
        foreach($this->load_forms($filters) as $form) {
            if (!is_null($form)) {
                $form['added'] = date("d.m.Y H:i", strtotime($form['added']));
                $forms[sha1(uniqid())] = $form;
            }
        } 
        
        // Get counts
        $all_count = (int)$wpdb->get_var(
                "
                SELECT count(`id`)
                FROM ".leadGenerator::$tablename." 
                "
        );         
        
        $active_count = (int)$wpdb->get_var(
                "
                SELECT count(`id`)
                FROM ".leadGenerator::$tablename." 
                WHERE `active` = 1
                "
        );
        
        $inactive_count = $all_count - $active_count;
        
        // Add modals api
        add_thickbox();

        $adapters = $this->adapters;
        
	// Add page content
        include LG__TEMPLATES_DIR.'home.template';
                
    }
    
    public function get_form_html($form){
        $adapters = $adapters = $this->adapters;

        // Unpack serialized data
        $form['adapter'] = unserialize($form['adapter']);
        $form['fields'] = unserialize($form['fields']);        
        
        // Let output to buff and pull it to variable
        ob_start();
            include(LG__TEMPLATES_DIR.'forms.list.item.template');
            $result = ob_get_contents();
        ob_end_clean();

 
        return $result;
    }
    
    protected function load_forms($filters){
        global $wpdb;
        $result = $wpdb->get_results( "SELECT * FROM ".self::$tablename." ORDER BY `id`", ARRAY_A);
        return $result;
    }    
    
    public function load_form($ident){
        global $wpdb;

        $sql = "SELECT * FROM ".self::$tablename." WHERE `ident` = %s LIMIT 1";
        $result = $wpdb->get_row( $wpdb->prepare($sql, $ident), ARRAY_A );                
        if (!(bool)$result) { // Check form exists
            return false;
        }            
        
        // Unpack serialized data
        $result['adapter'] = unserialize($result['adapter']);
        $result['fields'] = unserialize($result['fields']); 
        
        return $result;
    }      
    
    public function process_sc($attrs){
        include LG__TEMPLATES_DIR.'frontend.form.template';
    }
    
    
    // AJAX Wrapper for process_form method of this class
    public static function ajax_process_form(){    
        // Check nonce
        if( ! wp_verify_nonce( $_REQUEST['nonce'], 'ajax_validation' ) ) wp_die();

        $errors = array(); // Empty errors array        
        $data = self::getInstance()->process_form();
        
        switch($_REQUEST['format']) {
           case 'string' :
               echo $data;
               break;
           default : echo self::getInstance()->ajax_response($data, $errors); 
        }
        
        wp_die();  
    }    
    
    public function process_form(&$errors = null){
        global $wpdb;        

        // Check capability
        if (!current_user_can('manage_options')) {
            $errors[] = 'Insufficient rights to perform an operation';
        }
        
        if (!count($errors)) // No errors only
        switch($_REQUEST['do']){
            case 'save' :
                // Select adapter
                $hash = trim($_REQUEST['form_settings']['adapter']);
                $adapter = self::getInstance()->adapters[$hash];
                if ($adapter instanceof lg_Default_Adapter) {
                    $data = $adapter->process('form_save', $errors);
                } else {
                    $errors[] = 'Adapter was not found';
                }
            break;
            case 'remove' : 
                $form = $wpdb->get_row( "SELECT * FROM ".self::$tablename." WHERE `id` = ".(int)$_REQUEST['uid']." LIMIT 1");                
                if (!is_object($form)) { // Check form exists
                    $errors[] = 'Form was not found';
                    $data = false;
                    break;
                }
                
                $sql = "DELETE FROM ".self::$tablename." WHERE `id` = %d LIMIT 1";
                $data = (bool)$wpdb->query($wpdb->prepare($sql, (int)$_REQUEST['uid']));                
                break;
            case 'edit' : // Get edit form
                $form = $this->load_form($_REQUEST['ident']);
                $adapters = $this->adapters;
                $current_adapter = $this->get_adapter_by_hash($form['hash']);

                if (!is_null($form)) { // Check form exists
                    ob_start();
                        include(LG__TEMPLATES_DIR.'/form.edit.template');
                    $data = ob_get_contents();
                    ob_end_clean();
                    break;
                }
                $data = 'Form was not found';
                break;
            case 'activate' : 
                $data = array('active' => true);
                return (bool)$wpdb->update( 
                                self::$tablename,
                                $data,
                                array( // where
                                    'id' => (int)$_REQUEST['uid'],
                                ), null, null
                            );                 
                break;
                
            case 'deactivate' :
                $data = array('active' => false);
                return (bool)$result =   $wpdb->update( 
                                self::$tablename,
                                $data,
                                array( // where
                                    'id' => (int)$_REQUEST['uid'],
                                ), null, null
                            );
        }
        
        return $data;
    }
    
    // AJAX Wrapper for frontend request
    public static function ajax_process_frontend(){    
        // Check nonce
        if( ! wp_verify_nonce( $_REQUEST['nonce'], 'ajax_frontend_validation' ) ) wp_die();

        $errors = array(); // Empty errors array        
        $data = self::getInstance()->process_frontend($errors);
        echo self::getInstance()->ajax_response($data, $errors);        
        wp_die();  
    } 

    public function process_frontend(&$errors = null){
        global $wpdb;
        
            $form = $this->load_form($_REQUEST['ident']);
            if (is_null($form)) { // Check form exists
                $errors[] = 'Form was not found';
                return false;
            }
        
            
            // Select adapter
            $hash = $form['hash'];
            $adapter = self::getInstance()->adapters[$hash];
            if (!($adapter instanceof lg_Default_Adapter)) {
                $errors[] = 'Adapter was not found';
                return false;
            }
            
        return $adapter->process('form_send', $errors, $form);
    }
}
 
 