jQuery(document).ready(function(){
    // Show/Hide fieldsets
    jQuery(document).on('click', '.lg_form fieldset legend', function(){
        let fs = jQuery(this).closest('fieldset');
        fs.find('div.wrap').toggle(500, function(){
            if(jQuery(this).is(":hidden")) {
                fs.addClass('closed');
            }else {
                fs.removeClass('closed');
            }
        });
    })
    
    // Get settings form for current adapter
    jQuery(document).on('change', '.lg_form select[name="form_settings\[adapter\]"]', function(e){
        if(!confirm('Настройки адаптера будут сброшены! \nВы уверены, что хотите сменить адаптер?')) {
            // Return previous options
            jQuery(this).find('option.selected').attr('selected', true);
            return false;
        }
        
        var form = jQuery(this).closest('form');
        var sel  = jQuery(this).find('option:selected');
        let hash = sel.val();

        jQuery.getJSON(ajax.url+'/?action=adapter&do=form_get&hash='+hash+'&nonce='+ajax.nonce, function(data) {
            if (data != null && data != false && typeof(data) == 'object') {
                
                if (data['errors'].length > 0) {
                    alert('An error occurred');
                } else {
                    // Remove 'selected' class from all options
                    sel.parent().find('option').removeClass('selected');
                    sel.addClass('selected'); // And add 'selected' to selected 
                    
                    // Replace adapter settings form
                    form.find('.wrap.adapter').html(data['data']['settings']);
                }
                
            }
        });
    });  
    
    
    // Select field type
    jQuery(document).on('change', '.lg_form .field .type_selector', function(e){
        
        var cont = jQuery(this).closest('.field');
        var sel  = jQuery(this).find('option:selected');
        let hash = jQuery(this).closest('form').find('select[name="form_settings\[adapter\]"] option:selected').val();
        jQuery.getJSON(ajax.url+'/?action=adapter&do=field_get&type='+sel.val()+'&hash='+hash+'&nonce='+ajax.nonce, function(data) {
            if (data != null && data != false && typeof(data) == 'object') {
                if (data['errors'].length > 0) {
                    alert('An error occurred');
                } else {                   
                    // Replace adapter settings form
                    cont.replaceWith(jQuery(data['data']['html']));
                }
                
            }
        });
    });
    
    // Submit form
    jQuery(document).on('submit', '.lg_form', function(event){
        event.preventDefault();
        if(!confirm('Вы уверены, что хотите сохранить форму?')) return false;
        
        var form = jQuery(event.target);
        let fdata = new FormData(form[0]);
        
        fdata.append('action', 'form');
        fdata.append('do', 'save');
        fdata.append('nonce', ajax.nonce);
        
        jQuery.ajax(ajax.url, {
            type        : 'POST',
            async       : true,
            data        : fdata,
            dataType    : 'json',
            processData : false,
            contentType : false
        }).done(function(data){
            if (data != null && data != false && typeof(data) == 'object')
            if (data['data'] !== false){
                
                var html = jQuery(data['data']['html']);
                switch (data['data']['status']) {
                    case 'new' : 
                            jQuery('#the-list').append(html);
                            
                            // Increment counters
                            jQuery('.active .count, .all .count').each(function(){
                                jQuery(this).html(parseInt(jQuery(this).html())+1);
                            });
                            
                        break;
                    case 'updated' :                       
                            jQuery('.forms_list-item[data-uid="'+data['data']['id']+'"]').replaceWith(html);
                        break;  
                }
                
                // Add some animation
                html.addClass('highlighted');
                setTimeout(function(){
                    html.removeClass('highlighted');
                }, 1000);                
                
                /*
                if(form.find('input[name=autoclear]').is(':checked')) {
                    // Очищаем форму
                }
                */
                console.log(form.find('input[name=autoclose]'));
                if (form.find('input[name=autoclose]').length > 0){
                    if(form.find('input[name=autoclose]').is(':checked')) {
                        tb_remove();
                    }
                } else {
                    tb_remove();
                }
            }
        });
        
        
    });  
    
    // Remove one item
    jQuery(document).on('click', '.forms_list-item .delete', function(e){
        e.preventDefault();
        if(!confirm('Вы уверены, что хотите удалить форму?')) return false;
        
        let cont = jQuery(e.target).closest('.forms_list-item');
        remove_form(cont.data('uid'));
    });
    
    // Remove one item
    jQuery(document).on('click', '.delete_selected', function(e){
        e.preventDefault();
        if (jQuery('.forms_list-item .check-column input[type="checkbox"]:checked').length > 0){
            if(!confirm('Вы уверены, что хотите удалить отмеченные формы?')) return false;

            jQuery('.forms_list-item .check-column input[type="checkbox"]').each(function(){
                if (jQuery(this).is(':checked')) { // Remove only checked
                    let cont = jQuery(this).closest('.forms_list-item');
                    remove_form(cont.data('uid'))
                }
            });
        }
    });    
    
    
    // Add new field
    jQuery(document).on('click', '.add_field', function(e){
        e.preventDefault();
        let form = jQuery(e.target).closest('form');
        
        // Get adapter hash
        let hash = form.find('select[name="form_settings\[adapter\]"] option:selected').val();
        
        jQuery.getJSON(ajax.url+'/?action=adapter&do=field_get&hash='+hash+'&nonce='+ajax.nonce, function(data) {
            if (data != null && data != false && typeof(data) == 'object')
            if (data['data'] !== false){
                form.find('.fields table tbody').append(data['data']['html']);
            }
        }); 
        
    });
    
    jQuery(document).on('click', '.column-actions span.activate', function(e){
        e.preventDefault();
        let id = jQuery(e.target).closest('.forms_list-item').data('uid');
        jQuery.getJSON(ajax.url+'/?action=form&do=activate&uid='+id+'&nonce='+ajax.nonce, function(data) {
            if (data != null && data != false && typeof(data) == 'object')
            if (data['data'] === true){
                jQuery(e.target).closest('.forms_list-item').addClass('active');
                jQuery(e.target).parent().removeClass('activate').addClass('deactivate');
                jQuery(e.target).replaceWith('<a href="" class="edit" aria-label="Деактивировать форму">Деактивировать</a>');
            }
        })        
    });
    
    jQuery(document).on('click', '.column-actions span.deactivate', function(e){
        e.preventDefault();

        let id = jQuery(e.target).closest('.forms_list-item').data('uid');
        jQuery.getJSON(ajax.url+'/?action=form&do=deactivate&uid='+id+'&nonce='+ajax.nonce, function(data) {
            if (data != null && data != false && typeof(data) == 'object')
            if (data['data'] === true){
                jQuery(e.target).closest('.forms_list-item').removeClass('active');
                jQuery(e.target).parent().removeClass('deactivate').addClass('activate');
                jQuery(e.target).replaceWith('<a href="" class="edit" aria-label="Активировать форму">Активировать</a>');
            }
        })    
    });
    
    
    
    // Delete fields
    jQuery(document).on('click', '.delete_fields', function(e){
        e.preventDefault();
        if(!confirm('Вы уверены, что хотите удалить отмеченные поля?')) return false;        
        
        jQuery('.fields .check .lg-select').each(function(){
            if(jQuery(this).is(':checked')){
                jQuery(this).closest('.field').remove();
            }
        });
    });
    
    
    // Check all boxes
    jQuery(document).on('click', 'input[type=checkbox].lg-select-all', function(){
        var hndl = jQuery(this);
        var cont = jQuery(this).closest('table');
        cont.find('input[type=checkbox].lg-select').each(function(){
            jQuery(this).attr('checked', hndl.is(':checked'));
        });
    });  
    
    // Toggle fields state
    jQuery(document).on('click', '.field_disable', function(){
        let ch = jQuery(this).find('input');
        let cont = jQuery(this).closest('div');
        cont.find('input[type=text]').attr('disabled', (ch.is(':checked') ? false : true));
    });     
});


    
// Remove form
function remove_form(id) {
    var html = jQuery('.forms_list-item[data-uid="'+id+'"]');
    jQuery.getJSON(ajax.url+'/?action=form&do=remove&uid='+id+'&nonce='+ajax.nonce, function(data) {
        if (data != null && data != false && typeof(data) == 'object')
        if (data['data'] === true){
            // Add some animation
            html.addClass('redlighted');
            setTimeout(function(){
                html.remove();
            }, 1000);                 
        }
    });       
} 