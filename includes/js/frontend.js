    // Submit form
    jQuery(document).on('submit', '.lg_form', function(e){
        e.preventDefault();

        var form = jQuery(event.target);
        let fdata = new FormData(form[0]);
        
        fdata.append('action', 'frontend');
        fdata.append('do',      'submit');
        fdata.append('nonce', ajax.nonce);

        removeErrors(form);

        jQuery.ajax(ajax.url, {
            type        : 'POST',
            async       : true,
            data        : fdata,
            dataType    : 'json',
            processData : false,
            contentType : false
        }).done(function(data){
            if (data != null && data != false && typeof(data) == 'object')
            if (data['data'] !== false){
                form.replaceWith(jQuery('<div />',{
                    html : data['data']
                }));
            }else {
                showErrors(form, data['errors']);
            }
        });
        
        
});

function removeErrors(form){
    form.find('.error').remove();
    form.find('*').removeClass('error-highlight');
}

function showErrors(form, errors){
    jQuery.each(errors, function(key, value) {
        let e = form.find('*[name='+key+']').addClass('error-highlight');
        if (form.find('.error_'+key).length > 0)  {
            form.find('.error_'+key).html(value);
        } else {
            jQuery('<span />', {
                class: 'error',
                html : value
            }).insertBefore(e);
        }
    });            
}