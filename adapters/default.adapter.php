<?php
require_once LG__ADAPTERS_DIR.'adapter.interface.php';

class lg_Default_Adapter implements lg_Adapter_interface {
    protected $name         = 'Default Adapter';
    protected $description  = 'Базовые уведомления на почту';
    protected $version      = '1.0';
    
    public function process($do, &$errors, $data = null /* mixed data*/){
        switch ($do) {
            case 'form_get' : 
                return $this->form_process('get', $errors);                
                break;
            case 'form_save' :
                return $this->form_process('save', $errors);
                break;
            case 'form_send' :
                return $this->form_process('send', $errors, $data);
                break;            
            case 'field_get' :
                return $this->field_process('get', $errors);
                break;
        }
        return false;
    }
    
    public function get_name(){
        return ($this->name) ? $this->name : 'unknown';
    }

    public function get_description(){
        return ($this->description) ? $this->description : '';
    }   
    
    public function get_version(){
        return ($this->version) ? $this->version : 'unknown';
    }    
    
    // Returns unique hash based on classname
    public function get_hash(){
        return sha1(get_class($this));
    }    
    
    public function get_settings_form($settings = null){
        $adm_email = get_option( 'admin_email');
        $admin_notify = ((bool)$settings['admin_notification'] || is_null($settings)) ? 'checked' : '';
        $over_notify = ((bool)$settings['over_notification']) ? 'checked' : '';
        $over_is_disabled = ((bool)$settings['over_notification']) ? '' : 'disabled';
                
        $form =                
<<<HTML
    <div class="input-group-inline">
        <label id="check_00"><input type="checkbox" name="adapter_settings[admin_notification]" {$admin_notify} />Уведомление администратору ({$adm_email})</label>
    </div>   
    <div class="input-group">
        <label id="check_01" class="field_disable"><input type="checkbox" name="adapter_settings[over_notification]" {$over_notify}/>Уведомлять дополнительные адреса</label>
        <input value="{$settings['additional_addresses']}" type="text" name="adapter_settings[additional_addresses]" placeholder="Введите адреса через запятую: admin@example.com, user@example.com и т.д." {$over_is_disabled} />    
    </div>           
    <div class="input-group">
        <label>Заголовок письма</label>
        <input value="{$settings['mail_subject']}" type="text" name="adapter_settings[mail_subject]" value="Уведомление о новом лиде"/>
    </div>         
HTML;
        
        return $form;
    }
    
    public function get_fields(){
        return '';
    }
    
    public function form_process($action, &$errors, $data = null /* mixed data*/){
        global $wpdb;
        switch ($action) {
            case 'get' : 
                return array('settings' => $this->get_settings_form(), 'fields' => $this->get_fields());
                break;
            
            case 'save' : 
                $fs = $_REQUEST['form_settings'];
                $as = $_REQUEST['adapter_settings'];
                $fields = $_REQUEST['fields'];
                
                /*
                    ДОБАВИТЬ ПРОВЕРКУ ДАННЫХ
                */
                
                // Search for exist record
                $record_id = $wpdb->get_var(
                        "
                        SELECT `id`
                        FROM ".leadGenerator::$tablename." 
                        WHERE `ident` = '{$fs['ident']}'
                        "
                );                

                $form = array( // 'название_колонки' => 'значение'
                            'active'        => true,
                            'hash'          => $fs['adapter'],
                            'title'         => $fs['title'], 
                            // Make form identifier, if not exist
                            'ident'         => (!empty($fs['ident'])) ? $fs['ident'] : 'form_'.uniqid(),
                            'description'   => $fs['description'],
                            'adapter'       => serialize($as),
                            'fields'        => serialize($fields),
                            'agree'         => !empty($fs['agreement_check']),
                            'agreement'     => $fs['agreement_link']
                        );            
                
                $var_types =array( 
                                    '%d',
                                    '%s',
                                    '%s',
                                    '%s',
                                    '%s',
                                    '%s',
                                    '%s',
                                    '%d',
                                    '%s'                                
                            );
                        
                if (!(bool)$record_id) { // Make new record
                
                    $wpdb->insert(leadGenerator::$tablename, $form, $var_types);                   

                    $result['status'] = 'new'; 
                    
                } else { // Update record
                    $wpdb->update( 
                            leadGenerator::$tablename,
                            $form,
                            array( // where
                                    'ident' => $fs['ident'],
                            ), null, null
                    );                    
                    
                    $result['status'] = 'updated';                    
                }
                
                // Load new form data
                $form = leadGenerator::getInstance()->load_form($form['ident']);
                if (!$form) {                
                    $errors[] = 'Cannot fetch form data';
                    return false;
                }
                
                $form['added']  = date("d.m.Y H:i", strtotime($form['added']));
                $result['id'] = $form['id'] ;
                $result['html'] = leadGenerator::getInstance()->get_form_html($form);
                return $result;
            case 'send' :
                
                // Prepare fields data, return if errors
                $this->prepareData($data, $errors);
                if ((bool)count($errors)) {
                    return false;
                }
                
                // Send to destination by frontend request
                return ($this->form_send($data, $errors)) ? 'Спасибо. Форма была отправлена' : false;
        }

        return false;
    }
 
    public function field_process($action, &$errors){    
        
        switch($action) {
            case 'get' : 
                $result = array('html' => $this->backend_field_get($_REQUEST['type']));
                if (!$result) {
                    $errors[] = 'Cannot get field';
                    break;
                }
                return $result;
        }
        return false;
    }
    
    // Get preparet backend field, data is unnessesary
    public function backend_field_get($type, $data = null){
     
        $data['type'] = $type;
            
        $data['field_id'] = (empty($data['field_id'])) ? uniqid() : $data['field_id'];
        if (method_exists($this, 'backend_field_'.$type)){
            return call_user_func(array($this, 'backend_field_'.$type), $data);
        }
        
        return $this->backend_field_default($data);
    }
    
    
    /* Field generation functions */
    
    public function get_list_of_types(){
        return array(
            'text' => 'Тестовое поле',
            'email' => 'Поле для Email',
            'url' => 'Поле для URL',
            'phone' => 'Номер телефона',
            'select' => 'Выбираемое поле',
            /*'selected' => 'Выбранное поле',*/
        );
    }
    
    public function try_to_get_title_by_type($type){
        return $this->get_list_of_types()[$type];
    }
    
    // Backend
    protected function backend_field_default($data = null){

        ob_start();
            include(LG__TEMPLATES_DIR.'/fields/backend.default.template');
        
        $result = ob_get_contents();
        ob_end_clean();
        return $result;        
    }
    
    protected function backend_field_email($data = null){
        ob_start();
            include(LG__TEMPLATES_DIR.'/fields/backend.email.template');
        
        $result = ob_get_contents();
        ob_end_clean();
        return  $result;        
    }    
    
    protected function backend_field_phone($data = null){
        ob_start();
            include(LG__TEMPLATES_DIR.'/fields/backend.phone.template');
        
        $result = ob_get_contents();
        ob_end_clean();
        return  $result;        
    }       
    
    protected function backend_field_select($data = null){
        ob_start();
            include(LG__TEMPLATES_DIR.'/fields/backend.select.template');
        
        $result = ob_get_contents();
        ob_end_clean();
        return  $result;        
    }        

    protected function backend_field_selected($data = null){
        ob_start();
            include(LG__TEMPLATES_DIR.'/fields/backend.selected.template');
        
        $result = ob_get_contents();
        ob_end_clean();
        return  $result;        
    }        

    
    // Frontend
    public function frontend_field_get($field){
        
        // Prepare fields
        $field['name'] = (!empty($field['name'])) ? $field['name'] : 'field_'.(int)++$c;
        
        // Prepare function name
        $method_name = 'frontend_field_'.$field['type'];

        if (method_exists($this, $method_name)){
            return call_user_func(array($this, $method_name), $field);
        }
        
        return $this->frontend_field_default($field);
    }    
    
    protected function frontend_field_default($field){
        return '<input type="text" name="'.$field['name'].'" value="'.$field['content'].'" placeholder="'.$field['title'].'" />';        
    }
    
    protected function frontend_field_tel($field){
        return '<input type="tel" name="'.$field['name'].'" value="'.$field['content'].'" placeholder="'.$field['title'].'" />';
    }
    
    protected function frontend_field_email($field){
        return '<input type="email" name="'.$field['name'].'" value="'.$field['content'].'" placeholder="'.$field['title'].'" />';
    }
    
    protected function frontend_field_url($field){
        return '<input type="url" name="'.$field['name'].'" value="'.$field['content'].'" placeholder="'.$field['title'].'" />';
    }

    protected function frontend_field_select($field){
        $variants = explode("\n", $field['content']);
        foreach($variants as $variant) {
            $variant = explode('==', $variant);
            $key = trim((empty($variant[1])) ? $variant[0] : $variant[1]);
            $value = trim($variant[0]);
            $options .= "<option value=\"$key\">{$value}</option>";
        }
        return '<select name="'.$field['name'].'" />'.$options.'</select>';
    }    
    
    protected function frontend_field_selected($field){
        $variants = explode("\n", $field['content']);
        foreach($variants as $variant) {
            $variant = explode('==', $variant);
            $key = trim((empty($variant[1])) ? $variant[0] : $variant[1]);
            $value = trim($variant[0]);
            $options .= "<option value=\"$key\">{$value}</option>";
        }
        return '<select name="'.$field['name'].'" />'.$options.'</select>';
    }     
    
    public function validate_field($field, &$errors = null){
        if ($field['required'] && empty($field['value'])) {
            $errors[$field['name']] = 'Это поле обязательно к заполнению';
            return false;                     
        }        
        switch ($field['type']) {           
            case 'email' :
                if(!empty($field['value']) && !preg_match(' /^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/', $field['value'])) {
                    $errors[$field['name']] = 'E-Mail некорректен';
                    return false;     
                }
                break;
            case 'url' :
                if(!empty($field['value']) && !preg_match('/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/', $field['value'])) {
                    $errors[$field['name']] = 'URL некорректен';
                    return false;     
                }
                break;
            case 'tel' :
                if(!empty($field['value']) && !preg_match('/^([0-9\(\)\/\+ \-]*)$/i', $field['value'])) {
                    $errors[$field['name']] = 'Номер телефона некорректен';
                    return false;     
                }
            case 'select' :
                
                break;            
            default :
                
        }
            
        return true;
    }
    
    // Send form to destination
    protected function form_send($data, &$errors){
        $to = array();
        if ((bool)$data['adapter']['admin_notification']) {
            $to[] =  get_option( 'admin_email');
        }
        
        if ((bool)$data['adapter']['over_notification']) {
            $add_to = explode(',', $data['adapter']['additional_addresses']);
            $to = array_merge($to, (array)$add_to);
        }
        
        if (!count($errors)) {
            if (count($to)) // If there are recipients and have not errors
            if (!$this->sendMail(implode(',', $to), $data['adapter']['mail_subject'], $data)) {
                $errors[] = 'Failed to send form email';
                return false;
            }
            return true;
        }
        
        return false;
    }
    
    protected function prepareData(&$data, &$errors){
        $request = $_REQUEST;
        
        $as     = &$data['adapter'];
        $fields = &$data['fields'];
        
        $data['qs'] = sanitize_text_field($_REQUEST['qs']);
        
        if ($data['agree']) {
            if (!(bool)$_REQUEST['agree']){
                $errors['agree'] = 'Вы должны согласиться с условиями';
            }
        };
        
        // Check and fill all fields      
        array_walk($fields, function(&$field)use (&$errors){
            
            if (!$field['formit']) {
                $field['value'] = $field['content']; 
            }else {
                $field['value'] = $_REQUEST[$field['name']]; 
            }
            
            $this->validate_field($field, $errors);
        });
        
        return true;
    }

    /* Mails */
    protected function prepareMessage($data) {
               
    	// Get mail template
        ob_start();
        
            include(LG__TEMPLATES_DIR.'/default.mail.template');

        $result = ob_get_contents();
        ob_end_clean();        
        return $result;
    }
    
    // Send mail with lead infoirmation to all addresses
    protected function sendMail($to, $subject, $data = null){
            add_filter('wp_mail_content_type', create_function('', 'return "text/html";'));
            
            //Trying send mail with WP MAIL
            $result = wp_mail($to, $subject, $this->prepareMessage($data));
            if (!$result) { // IF WP MAIL DOES NOT WORK, USE BASIC MAIL SENDER!
                $result = mail($to, $subject, $this->prepareMessage($data));
            }
        return $result;
    }
    
}