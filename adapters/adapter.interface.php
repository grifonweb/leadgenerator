<?php

interface lg_Adapter_interface {
    public function get_name(); // Get adapter name
    public function get_hash(); // Get adapter identifier
    public function get_version(); // Get adapter version
    public function process($do, &$errors); // Entry point for ajax requests
}