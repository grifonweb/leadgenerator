<?php
require_once LG__ADAPTERS_DIR.'default.adapter.php';

class lg_Bx24_Adapter extends lg_Default_Adapter {
    protected $name         = 'BX24 Adapter';
    protected $description  = 'Создание лида в системе Bitrix 24';
    protected $version      = '0.6';  
    
    public function get_settings_form($settings = null){
        $parent = parent::get_settings_form($settings);
        
        $crm_api = ($settings['crm_api']) ? $settings['crm_api'] : '/crm/configs/import/lead.php' ;
        $crm_host = ($settings['crm_host']) ? $settings['crm_host'] : '' ;
        $crm_port = ($settings['crm_port']) ? (int)$settings['crm_port'] : 443;
        $lead_name = ($settings['lead_title']) ? $settings['lead_title'] : 'Лид с сайта: '.get_option('siteurl') ;
        
        ${$settings['crm_protocol'].'_sel'} = 'selected';
        
        $form = 
<<<HTML
    <div class="input-group-inline">
            <select name="adapter_settings[crm_protocol]">
                <option value="https" {$http_sel}>https</option>
                <option value="ssl" {$ssl_sel}>ssl</option>
            </select>
    </div>
    <div class="input-group-inline">
        <label>Хост</label><br />
        <input value="{$crm_host}" type="text" name="adapter_settings[crm_host]" placeholder="Хост на котором находится ваш Bitrix24 (yourdomain.bitrix24.ru)" size="79"/>
    </div>
    <div class="input-group-inline">
        <label>Порт</label><br />
        <input value="{$crm_port}" type="text" name="adapter_settings[crm_port]" placeholder="Порт Bitrix24" size="6"/>
    </div>  
    <div class="input-group">
        <label>rest api path</label>
        <input value="{$crm_api}" type="text" name="adapter_settings[crm_api]" placeholder="Пусть к обработчику запросов Bitrix24 (yourdomain.bitrix24.ru)"/>
    </div>        
        <hr />
        Обязательные поля<br />      
    <div class="input-group-inline">
        <label>Логин</label><br />
        <input value="{$settings['crm_login']}" type="text" name="adapter_settings[crm_login]" placeholder="Ваш логин в Bitrix2"/>
    </div>                
    <div class="input-group-inline">
        <label>Пароль</label><br />
        <input value="{$settings['crm_passw']}" type="password" name="adapter_settings[crm_passw]" placeholder="Ваш пароль в Bitrix24"/>
    </div>

    <div class="input-group">
        <label>Заголовок</label>
        <input value="{$lead_name}" type="text" name="adapter_settings[lead_title]" placeholder="Название лида"/>
    </div>        
HTML;
        
        return $parent.$form;
    }
    
    protected function form_send($data, &$errors) {
        // First of all, we will send to CRM
        if (!$this->send_to_crm($data, $errors)){
            return false;
        }
        
        // And to mail if have'nt errors
        if (!count($errors)){
            return parent::form_send($data, $errors);
        }
        
        return false;
    }
    
    /* Field generation functions */
    
    public function get_list_of_types(){
        return array(
            'text' => 'Стандартное текстовое поле',
            'email' => 'Стандартный адрес email',
            'url' => 'Стандартное поле URL',
            'phone' => 'Стандатрное поле телефона',
            'select' => 'Стандартный список выбора',            
            'COMPANY_TITLE' => 'Название компании',
            'NAME' => 'Имя',
            'LAST_NAME' => 'Фамилия',
            'SECOND_NAME' => 'Отчество',
            'POST' => 'Должность',
            'ADDRESS' => 'Адрес',
            'COMMENTS' => 'Комментарий',
            'SOURCE_DESCRIPTION' => 'Дополнительно о статусе',
            'STATUS_DESCRIPTION' => 'Дополнительно об источнике',
            'OPPORTINUTY' => 'Возможная сумма сделки',
            'CURRENCY_ID' => 'Валюта',
            'PRODUCT_ID' => 'Продукт',
            'SOURCE_ID' => 'Источник',
            'STATUS_ID' => 'Статус',
            'ASSIGNED_BY_ID' => 'Ответственный',
            'PHONE_WORK' => 'Рабочий телефон',
            'PHONE_MOBILE' => 'Мобильный телефон',
            'PHONE_FAX' => 'Номер факса',
            'PHONE_HOME' => 'Домашний телефон',
            'PHONE_PAGER' => 'Номер пейджера',
            'PHONE_OTHER' => 'Другой телефон',
            'WEB_WORK' => 'Корпоративный сайт',
            'WEB_HOME' => 'Личная страница',
            'WEB_FACEBOOK' => 'Страница Facebook',
            'WEB_LIVEJOURNAL' => 'Страница LiveJournal',
            'WEB_TWITTER' => 'Микроблог Twitter',
            'WEB_OTHER' => 'Другой сайт',
            'EMAIL_WORK' => 'Рабочий e-mail',
            'EMAIL_HOME' => 'Частный e-mail',
            'EMAIL_OTHER' => 'Другой e-mail',
            'IM_SKYPE' => 'Контакт Skype',
            'IM_ICQ' => 'Контакт ICQ',
            'IM_MSN' => 'Контакт MSN/Live!',
            'IM_JABBER' => 'Контакт Jabber',
            'IM_OTHER' => 'Другой контакт',
        );
    }    
    
    protected function backend_field_STATUS_ID($data = null){
        $variants = array(
            'NEW' => 'Не обработан',
            'ASSIGNED' => 'Назначен ответственный',
            'DETAILS' => 'Уточнение информации',
            'CANNOT_CONTACT' => 'Не удалось связаться',
            'IN_PROCESS' => 'В обработке',
            'ON_HOLD' => 'Обработка приостановлена',
            'RESTORED' => 'Сконвертирован',
            'CONVERTED' => 'Восстановлен',
            'JUNK' => 'Некачественный лид'
        );
        
        $data['variants'] = $variants;
        
        return parent::backend_field_selected($data);
    }
    
    protected function frontend_field_STATUS_ID($data = null){
        return  parent::frontend_field_selected($data);        
    }     
    
    
    protected function backend_field_SOURCE_ID($data = null){
        $variants = array(
            'SELF' => 'Свой контакт',
            'PARTNER' => 'Существующий клиент',
            'CALL' => 'Звонок',
            'WEB' => 'Веб-сайт',
            'EMAIL' => 'Электронная почта',
            'CONFERENCE' => 'Конференция',
            'TRADE_SHOW' => 'Выставка',
            'EMPLOYEE' => 'Сотрудник',
            'COMPANY' => 'Кампания',
            'HR' => 'HR - департамент',
            'MAIL' => 'Письмо',
            'OTHER' => 'Другое',
        );
   
        $data['variants'] = $variants;
        
        return parent::backend_field_selected($data);
    }
    
    protected function frontend_field_SOURCE_ID($data = null){
        return  parent::frontend_field_selected($data);        
    }    
    
    protected function backend_field_CURRENCY_ID($data = null){
        $variants = array(
            'RUB' => 'Рубль',
            'USD' => 'Доллар США',
            'EUR' => 'Евро',
        );
        
        $data['variants'] = $variants;
        
        return parent::backend_field_selected($data);
    }
    
    protected function frontend_field_CURRENCY_ID($data = null){
        return  parent::frontend_field_selected($data);        
    }      
    
    protected function backend_field_PRODUCT_ID($data = null){
        $variants = array(
            'PRODUCT_1' => '1С-Битрикс: Управление сайтом',
            'PRODUCT_2' => '1С-Битрикс: Корпоративный портал',
            'OTHER' => 'Другое',
        );

        $data['variants'] = $variants;
        
        return parent::backend_field_selected($data);
    }
    
    protected function frontend_field_PRODUCT_ID($data = null){
        return  parent::frontend_field_selected($data);        
    } 
    
    protected function prepare_post_data($data){       
        $result['LOGIN']    = sanitize_text_field($data['adapter']['crm_login']);
        $result['PASSWORD'] = sanitize_text_field($data['adapter']['crm_passw']);
        $result['TITLE']    = sanitize_text_field($data['adapter']['lead_title']);
        
        foreach ($data['fields'] as $field) {
            // If empty then skip
            if (empty($field['value'])) continue;
            
            $result[$field['name']] = sanitize_text_field($field['value']);
        }
        
        return $result;
    }
    
    protected function send_to_crm($data, &$errors){
        $post_data = $this->prepare_post_data($data);
        
        // Prepare UT Marks
        $q = array();
        parse_str($data['qs'], $q);
        foreach($q as $key => $val) {
            if (!array_key_exists($key, $post_data)) {
                $post_data[$key] = $val;
                //$post_data[strtoupper($key)] = $val;
            }
        }
        
        try {
                switch ($data['adapter']['crm_protocol']) {
                    case 'ssl' :
                        // open socket to CRM
                        $fp = fsockopen("ssl://".$data['adapter']['crm_host'], $data['adapter']['crm_port'], $errno, $errstr, 30);
                        if ($fp){
                                // prepare POST data
                                $str_post_data = '';
                                foreach ($post_data as $key => $value) {
                                        if ( strlen( $value ) > 512 ) {
                                                $value = substr( $value, 0, 512 );
                                        }
                                        $value = sanitize_text_field($value);
                                        $str_post_data .= ($str_post_data == '' ? '' : '&').$key.'='.urlencode($value);
                                }

                                // prepare POST headers
                                $str = "POST ".$data['adapter']['crm_api']." HTTP/1.0\r\n";
                                $str .= "Host: ".$data['adapter']['crm_host']."\r\n";
                                $str .= "Content-Type: application/x-www-form-urlencoded\r\n";
                                $str .= "Content-Length: ".strlen($str_post_data)."\r\n";
                                $str .= "Connection: close\r\n\r\n";

                                $str .= $str_post_data;

                                // send POST to CRM
                                fwrite($fp, $str);

                                // get CRM headers
                                $result = '';
                                while (!feof($fp))
                                {
                                        $result .= fgets($fp, 128);
                                }
                                fclose($fp);
                                $response = explode("\r\n\r\n", $result)[1];
                        }
                        else
                        {
                                $errors[] = 'Connection Failed! '.$errstr.' ('.$errno.')';
                                return false;
                        }
                        break;
                    default :
                        $str = http_build_query($post_data);
                        $result = file_get_contents('https://'.$data['adapter']['crm_host'].$data['adapter']['crm_api'], false, stream_context_create(array(
                            'http' => array(
                                'method' => 'POST',
                                'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                                    . "Content-Length: " . strlen($str) . "\r\n",
                                'content' => $str
                            )
                        )));  
                        $response = $result;
                }              
                
                $json_in = array("{'", "'}", "':'", "','");
                $json_out = array('{"', '"}', '":"', '","');
                $json = str_replace($json_in, $json_out, $response);
                $json_arr = json_decode($json, true);                
                
                switch ($json_arr['error']) {
                    case '201':
                        return true;
                }
                
                $errors[] = $json_arr['error_message'];
                return false;                        
        } catch(Exception $e) {
                $errors[] = var_export($e, true);
        }
        return false;
    }
}